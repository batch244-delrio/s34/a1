// This is used to get the contents of the express package to be used by the application
const express = require("express");

// Creates an application using express
// This creates an express application and stores it in a variable
// In layman's term, app is our server
const app = express();

// For our application server run , we need port to listen to
const port = 3000;

// Setup for allowing the server to handle data from request
// Allows our app to read JSON data
// Middleware is a request handler that has access to the applications request and response cycle
app.use(express.json())



let users = [
	{
		username: "TStark3000",
		email: "starkindustries@email.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@email.com",
		password: "iLoveStormBreaker"
	}
];


// [SECTION] Routes

// Express has methods corresponding to each HTTP method

// This route expect to receive a GET request at the base URI
// This will return a simple message back to the client
app.get("/", (req, res) => {

	// Combines writeHead() and end()
	// It used the express JS modules, method instead to send a response back to the client
	res.send(`Hello World`);
})


// MINI activity

app.get("/greeting", (req, res) => {
	res.send(`Hello from Batch244-Del Rio`)
});


// This routes expects to receive POST request

app.post(`/hello`, (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

// This route expects to receive a post request at the URI "/signup"

app.post("/signup", (req, res) => {
	if (req.body.username !== "" && req.body.password !== "" && req.body.email !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} is succesfully registered!`)
	} else {
		res.send(`Please input BOTH username and password.`)
	}
})

// Retrieving all users
app.get("/allUsers", (req, res) => {
	res.send(users)
})

// This route expects to recieve a PUT request
// This will update the password of a user that matches the information provided to postman

app.put("/changepass", (req, res) => {
	let message;

	for (let i = 0; i < users.length; i++){

		if (req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated`
			break;
		} else {
			message = "User does not exist."
		}	
	}

	res.send(message);
})

// Process a GET request at the "/home" route using postman.
app.get("/home", (req, res) => {
	res.send(`Welcome to the homepage`)
});
//  Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete('/delete-user', (req, res) => {

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === req.body.username) {
      users.splice(i, 1);
      break;
    }
  }

  res.json({ message: `User ${req.body.username} has been deleted` });
});


app.listen(port, () => console.log(`Server is running at port ${port}`))

